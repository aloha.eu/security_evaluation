Delete all points that don't apply to your question.

## *Who* is the bug affecting?
<!-- Ex. company or university -->

## *What* is affected by this bug?
<!-- Ex. specific module -->

## *When* does this occur?
<!-- Ex. Every time I run a security evaluation... -->

## *How* do we replicate the issue?
<!-- Please be specific as possible. Use dashes (-) or numbers (1.) to create a list of steps -->


## Expected behavior (i.e. solution)
<!-- What should have happened? -->


## Other Comments


## What can we do to improve our module?

## What should we document better?