swagger: "2.0"
info:
  version: "2.0"
  title: "Data and Model security evaluation"
  description: "This API performs the security evaluation of a machine learning model."
tags:
  - name: security-evaluation
    description: "Adversarial machine learning API."
  - name: adversarial-sample-generation
    description: "Generation of an adversarial demo image."
consumes:
  - application/json
produces:
  - application/json


paths:

  /security_evaluations:
    post:
      tags:
        - security-evaluation
      summary: "Posts a request to evaluate security of a trained deep-learning algorithm against adversarial (worst-case) input data manipulation, along with the necessary parameters."
      operationId: startEvaluation
      description: "Returns ID of the security-evaluation job. IDs are progressive and atomically assigned to a job request even if it immediately fails."
      parameters:
        - in: body
          name: "Parameters"
          description: "Parameters required for the performance evaluation."
          schema:
            $ref: '#/definitions/EvaluationParameters'
        - name: callback_id
          in: query
          description: Id to identify the callback
          required: true
          type: string
      responses:
        "202":
          description: "Accepted. The job is now in the queue and ready to process."
          headers:
            Location:
              description: "Location of the newly created job resource."
              type: string
          schema:
            type: string
            description: "ID of the security evaluation job."
            example: "job123"
        "400":
          description: "Bad request. Missing parameter."
        "404":
          description: "Model or dataset not found."
        "422":
          description: "Unprocessable entry."
    get:
      description: "Returns the list of all security evaluation jobs, with optional filtering parameters."
      tags:
        - security-evaluation
      summary: "Retrieves information about the running, queued and finished security evaluation jobs."
      operationId: getAllSecurityEvaluations
      parameters:
        - name: status
          in: query
          description: "Filter all evaluation jobs by status."
          type: string
      responses:
        "200":
          description: "Success. Returns the list of jobs with optional filtering."
          schema:
            type: array
            description: "List of queued, running and finished jobs."
            items:
              type: object
              description: "Job element."
              properties:
                id:
                  type: string
                  example: "job123"
                  description: "ID of the job."
                job-status:
                  type: string
                  example: "running"
                  description: "Status of the job."
        "400":
          description: "Invalid input."
    delete:
      description: "Cancel all security evaluation jobs."
      summary: "Deletes all queued jobs and stops the ones that are running. Finished jobs are however deleted automatically after the result timeout."
      tags:
        - security-evaluation
      operationId: deleteAllSecurityEvaluations
      parameters:
        - name: status
          in: query
          description: "Delete all jobs with the given status"
          type: string
      responses:
        "200":
          description: "Success. All jobs have been deleted."
        "400":
          description: "Invalid input."

  /security_evaluations/{id}:
    get:
      tags:
        - security-evaluation
      summary: "Retrieves the status of the given security evaluation job."
      description: "Returns the status of a security evaluation job started with a security evaluation POST request."
      operationId: getEvaluationJobStatus
      parameters:
        - name: "id"
          in: path
          description: "Id of the job started with a security evaluation POST request."
          required: true
          type: string
      responses:
        "200":
          description: "Success. Returns the status of the job identified by the specified job ID."
          schema:
            type: object
            properties:
              job-status:
                type: string
                example: "running"
            description: "Status of the job."
            example: "running"
        "303":
          description: "See other. The job is completed and results can be accessed."
          headers:
            Location:
              description: "Location of the results resource produced by the job."
              type: string
        "404":
          description: "Security evaluation ID not found."
    delete:
      tags:
        - security-evaluation
      summary: "Cancels a security evaluation job."
      description: "Deletes from the job queue the specified job ID."
      operationId: deleteSecurityEvaluationJob
      parameters:
        - name: "id"
          in: path
          description: "ID of the security evaluation job to delete."
          required: true
          type: string
      responses:
        "200":
          description: "Success. The job has been removed from the queue."
        "404":
          description: "Job ID not found."

  /security_evaluations/{id}/output:
    get:
      tags:
        - security-evaluation
      summary: "Retrieves the results of a security-evaluation job started with a POST request."
      operationId: getEvaluationJobResults
      parameters:
        - name: "id"
          in: path
          description: "ID of the job started with a security evaluation POST request."
          required: true
          type: string
      responses:
        "200":
          description: "Success. Returns synthetic measures summarizing the security evaluation, along with the complete security evaluation curve reporting the value of the given performance metric w.r.t each of the noise level values provided as input."
          schema:
            $ref: '#/definitions/SecurityEvaluationResults'
        "307":
          description: "Temporary redirect. The requested job resource is not available yet. Returns a short hypertext note with a hyperlink to the URI of the job status information."
          headers:
            Location:
              description: "Location of the job resource that is assigned to process the results."
              type: string
        "404":
          description: "Security evaluation ID not found."
        "410":
          description: "Gone. The requested job resource is no longer available at the server. "
    delete:
      tags:
        - security-evaluation
      summary: "Deletes results of a security evaluation.."
      description: "Deletes results of a finished security evaluation job."
      operationId: deleteSecurityEvaluationJobResults
      parameters:
        - name: "id"
          in: path
          description: "ID of the security evaluation job that processed the results to delete."
          required: true
          type: string
      responses:
        "200":
          description: "Success. Results have been removed from the server."
        "307":
          description: "Temporary redirect. The requested result resource is not available yet. Returns a short hypertext note with a hyperlink to the URI for a job cancel request."
        "404":
          description: "Job ID not found."

  /adversarial_samples:
    post:
      tags:
        - adversarial-sample-generation
      summary: "Posts a request to create an adversarial sample with a trained deep-learning algorithm."
      operationId: getAdversarialSample
      description: "Returns a newly created adversarial image, starting from an image in the given dataset, on the selected trained model. Format: png."
      parameters:
        - in: body
          name: "Parameters"
          description: "Parameters required for the performance evaluation."
          schema:
            $ref: '#/definitions/AdversarialExampleParameters'
      responses:
        "202":
          description: "Created."
          schema:
            type: string
            description: "ID of the security evaluation job."
            example: "job123"
        "404":
          description: "Model or dataset not found."
        "405":
          description: "Invalid input."
        "422":
          description: "Unprocessable entry."
    get:
      tags:
        - adversarial-sample-generation
      summary: "Retrieves information about the running, queued, failed and finished adversarial examples creations."
      operationId: getAdversarialSamplesStatus
      description: "Gets the status of all adversarial sample generation jobs."
      responses:
        "200":
          description: "OK."
          schema:
            type: array
            description: "List of queued, running and finished jobs."
            items:
              type: object
              description: "Job element."
              properties:
                id:
                  type: string
                  example: "job123"
                  description: "ID of the job."
                job-status:
                  type: string
                  example: "running"
                  description: "Status of the job."
        "400":
          description: "Invalid input."
    delete:
      description: "Cancel all adversarial sample generation jobs."
      summary: "Deletes all queued jobs and stops the ones that are running. Finished jobs are however deleted automatically after the result timeout."
      tags:
        - adversarial-sample-generation
      operationId: deleteAllAdvSampleGenerations
      parameters:
        - name: status
          in: query
          description: "Delete all jobs with the given status"
          type: string
      responses:
        "200":
          description: "Success. All jobs have been deleted."
        "400":
          description: "Invalid input."

  /adversarial_samples/{id}:
    get:
      tags:
        - adversarial-sample-generation
      summary: "Retrieves the status of the given adversarial sample generation job."
      description: "Returns the status of an adversarial sample generation job started with a previous POST request."
      operationId: getAdversarialSamplesJobStatus
      parameters:
        - name: "id"
          in: path
          description: "Id of the job started with a security evaluation POST request."
          required: true
          type: string
      responses:
        "200":
          description: "Success. Returns the status of the job identified by the specified job ID."
          schema:
            type: object
            properties:
              job-status:
                type: string
                example: "running"
            description: "Status of the job."
            example: "running"
        "303":
          description: "See other. The job is completed and results can be accessed."
          headers:
            Location:
              description: "Location of the results resource produced by the job."
              type: string
        "404":
          description: "Adversarial sample generation job ID not found."
    delete:
      tags:
        - adversarial-sample-generation
      summary: "Cancels an adversarial sample generation job."
      description: "Deletes from the job queue the specified job ID."
      operationId: deleteAdversarialSamplesJob
      parameters:
        - name: "id"
          in: path
          description: "ID of the adversarial sample generation job to delete."
          required: true
          type: string
      responses:
        "200":
          description: "Success. The job has been removed from the queue."
        "404":
          description: "Job ID not found."

  /adversarial_samples/{id}/output:
    get:
      tags:
        - adversarial-sample-generation
      summary: "Retrieves the results of an adversarial sample generation job started with a POST request."
      operationId: getAdversarialSamplesJobResults
      parameters:
        - name: "id"
          in: path
          description: "ID of the job started with a previous POST request."
          required: true
          type: string
      responses:
        "200":
          description: "Success. Returns a demo image of adversarial perturbation on a sample taken from the specified dataset."
          schema:
            type: file
            description: "PNG image of adversarial example."
        "307":
          description: "Temporary redirect. The requested job resource is not available yet. Returns a short hypertext note with a hyperlink to the URI of the job status information."
          headers:
            Location:
              description: "Location of the job resource that is assigned to process the results."
              type: string
        "404":
          description: "Adversarial sample generation ID not found."
        "410":
          description: "Gone. The requested job resource is no longer available at the server. "
    delete:
      tags:
        - adversarial-sample-generation
      summary: "Deletes results of an adversarial sample generation."
      description: "Deletes results of a finished adversarial sample generation job."
      operationId: deleteAdversarialSamplesJobResults
      parameters:
        - name: "id"
          in: path
          description: "ID of the adversarial sample generation job that processed the results to delete."
          required: true
          type: string
      responses:
        "200":
          description: "Success. Results have been removed from the server."
        "307":
          description: "Temporary redirect. The requested result resource is not available yet. Returns a short hypertext note with a hyperlink to the URI for a job cancel request."
        "404":
          description: "Job ID not found."

definitions:
  EvaluationParameters:
    type: object
    required:
      - dataset
      - trained-model
    properties:
      dataset:
        type: string
        description: "Name of the dataset to use for performance evaluation."
        example: "CIFAR10"
      trained-model:
        type: string
        description: "Path of the pre-trained model evaluate."
        example: "/data/resnet18/resnet18_pi123.onnx"
      performance-metric:
        type: string
        description: "Name of the performance metric to use."
        example: "classification-accuracy"
      perturbation-type:
        type: string
        description: "Type of the perturbation to use to generate adversarial examples."
        example: "max-norm"
      perturbation-values:
        type: array
        description: "List of values to use for generate the perturbations."
        items:
          type: number
        example: "[0, 1.0, 2.0, 5.0, 10.0]"

  AdversarialExampleParameters:
    type: object
    required:
      - dataset
      - trained-model
    properties:
      dataset:
        type: string
        description: "Name or MongoDB object ID of the dataset source of the sample image."
        example: "CIFAR10"
      trained-model:
        type: string
        description: "MongoDB object ID of the pre-trained model evaluate."
        example: "c78as6cdasjh3ds"
      performance-metric:
        type: string
        description: "Name of the performance metric to use."
        example: "classification-accuracy"
      perturbation-type:
        type: string
        description: "Type of the perturbation to use to generate adversarial examples."
        example: "max-norm"
      perturbation-values:
        type: array
        description: "List of values to use for generate the perturbations."
        items:
          type: number
        example: "[0, 1.0, 2.0, 5.0, 10.0]"

  SecurityEvaluationResults:
    type: object
    properties:
      sec-level:
        type: string
        description: "Resulting security level of the security evaluation."
        example: "high"
      sec-value:
        type: number
        example: "0.2"
        description: "Resulting score of the security evaluation. The range depends on the chosen metric."
      sec-curve:
        type: object
        description: "Complete security evaluation curve resulting from the security evaluation."
        properties:
          x-values:
            type: array
            items:
              type: number
            example: "[0, 1, 2, 3]"
          y-values:
            type: array
            items:
              type: number
            example: "[10, 15, 18, 20]"


# UNCOMMENT THIS ANS REMOVE AUTO MOCKING LINES
host: modelstore.aloha.eu
basePath: /api
schemes:
  - http
