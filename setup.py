from setuptools import find_packages, setup

setup(
    name='security_evaluation',
    package_dir={'': 'app'},
    packages=find_packages(where='app'),
)
