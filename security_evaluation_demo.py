import argparse
import os
import sys
import time

import warnings

warnings.filterwarnings("ignore", module="weasyprint")
warnings.filterwarnings("ignore", module="cleverhans")
warnings.filterwarnings("ignore", module="tensorflow")
warnings.filterwarnings("ignore", module="tensorboard")

import matplotlib.pyplot as plt
from jinja2 import Environment, FileSystemLoader

urls = {
    'classification': {'model': '1TqCYB-tVe7XdAByh5lsXNewnRqwWyoZr',
                       'dataset': '1ogdOWihTojV3HiGY-GySx7d-6QaP1ImU',
                       'pipeline': '1aqRRk3ntbnMWCAiWcO2u6nQgii0VN65c'},
    'detection': {'model': '1GltyP8ywABJ_JG18genhCfX0AxNmaf5a',
                  'dataset': '1__nEm38loEBxP_1uas6vgFvq5kSDL0xT',
                  'config': '118n7Ahzwoo7hyvmHfRLqvj4f8-AgxVTh'},
    'segmentation': {'model': '1aALNr74VXT2xR8e0rh6H76Xr-PMvk-zN',
                     'dataset': '12nTtaYnDeuxokbhJLMCh4eDsU2DJ10ny'}
}

available_tasks = ['classification', 'detection', 'segmentation']
available_modes = ['fast', 'complete']
available_perturbations = ['random', 'max-norm']
metrics = {'classification': 'classification-accuracy',
           'detection': 'map',
           'segmentation': 'iou'}

perturbation_values = {
    'classification': [0, 0.01, 0.02, 0.03, 0.05],
    'detection': [0, 0.001, 0.002, 0.003, 0.005],
    'segmentation': [0, 0.01, 0.02, 0.03, 0.05]
}

parser = argparse.ArgumentParser(description="Runs the demo of the "
                                             "security evaluation module.")
parser.add_argument('--task', choices=available_tasks,
                    help='Selection of the use case.')
parser.add_argument('--evaluation-mode', choices=available_modes,
                    help='Selection of the evaluation mode. Fast will run '
                         'the evaluation on a subset of samples and eps values.')
parser.add_argument('--noise-type', choices=available_perturbations,
                    help='Selection of the perturbation.')
parser.add_argument('--target-dir', default='./temp',
                    help='Directory where to store temporary data '
                         'and final results.')

args = parser.parse_args()

sys.path.append('app/')
from security_evaluations.security_evaluation_lib.utils import download_gdrive
from security_evaluations.evaluation_manager import EvaluationManager

# create dirs and download model and data
target_dir = args.target_dir
if not os.path.exists(target_dir):
    os.makedirs(target_dir)

task = args.task
task_dir = os.path.join(target_dir, task)
if not os.path.exists(task_dir):
    os.makedirs(task_dir)

model_path = os.path.join(task_dir, 'onnx_model.onnx')
dataset_path = os.path.join(task_dir, 'dataset.hdf5')

if not os.path.exists(model_path):
    download_gdrive(urls[task]['model'], model_path)

if not os.path.exists(dataset_path):
    download_gdrive(urls[task]['dataset'], dataset_path)

if 'pipeline' in urls[task]:
    pipeline_path = os.path.join(task_dir, 'pipeline.json')
    if not os.path.exists(pipeline_path):
        download_gdrive(urls[task]['pipeline'], pipeline_path)
else:
    pipeline_path = None

if 'config' in urls[task]:
    config_path = os.path.join(task_dir, 'config.json')
    if not os.path.exists(config_path):
        download_gdrive(urls[task]['config'], config_path)
else:
    config_path = None


# take starting time
start = time.time()

epsilons = perturbation_values[task]
# create evaluation manager
em = EvaluationManager(
    dataset_id=dataset_path,
    model_id=model_path,
    metric=metrics[task],
    perturbation_type=args.noise_type,
    evaluation_mode=args.evaluation_mode,
    task=task,
    perturbation_values=epsilons,
    preprocessing_pipeline=pipeline_path,
    config_file=config_path
)

# run security evaluation
sec_eval_data = em.sec_eval_curve()

# take final time
end = time.time()
elapsed = end - start
s = elapsed
num_samples = len(em._validation_loader)
seconds_per_sample = elapsed / num_samples
hours, remainder = divmod(s, 3600)
minutes, seconds = divmod(remainder, 60)
str_elapsed = '{:02} hours {:02} minutes {:02} seconds'.format(int(hours), int(minutes), int(seconds))

# create plots
out_folder = os.path.join(task_dir, 'outputs')
if not os.path.exists(out_folder):
    os.makedirs(out_folder)
out_folder = os.path.join(out_folder, args.noise_type)
if not os.path.exists(out_folder):
    os.makedirs(out_folder)

out_folder_images = os.path.join(out_folder, 'images')

if not os.path.exists(out_folder_images):
    os.makedirs(out_folder_images)

outimg = os.path.join(out_folder_images, 'sec_eval_plot.png')
outimg_pdf = os.path.join(out_folder_images, 'sec_eval_plot.pdf')

plt.figure()
plt.plot(sec_eval_data['sec-curve']['x-values'], sec_eval_data['sec-curve']['y-values'],
         color='tab:red')
plt.xlabel("Perturbation Strength", fontsize=20)
plt.xticks(fontsize=20, alpha=.7)
plt.ylabel("Model Accuracy", fontsize=20)
plt.yticks(fontsize=20, alpha=.7)
plt.ylim([0.0, 1.0])
# Remove borders
plt.gca().spines["top"].set_alpha(0.0)
plt.gca().spines["bottom"].set_alpha(0.3)
plt.gca().spines["right"].set_alpha(0.0)
plt.gca().spines["left"].set_alpha(0.3)
plt.grid()
plt.tight_layout()

plt.savefig(outimg)
plt.savefig(outimg_pdf, format='pdf')
plt.close()

# create final report with stats
env = Environment(loader=FileSystemLoader('app/static'))
template = env.get_template("report_template.html")
stats = (
    ('Task', task),
    ('Evaluation Mode', args.evaluation_mode),
    ('Noise Type', args.noise_type),
    ('Base accuracy', "%0.2f" % sec_eval_data['sec-curve']['y-values'][0]),
    ('Perturbation values', epsilons),
    ('Num samples', num_samples),
    ('Elapsed time', str_elapsed + f' ({seconds_per_sample:.2f} seconds / sample)'),
    ('Security Level', sec_eval_data['sec-level']),
    ('Security Value', "%0.2f" % sec_eval_data['sec-value'])
)

out_imgs = ["{}".format(eps).replace('.', '_') for eps in epsilons]
x, y = next(iter(em._validation_loader))
for (eps, out_img) in zip(epsilons, out_imgs):
    adv_points = em.generate_advx(x, y, float(eps))
    em.attack.generate_figure(x, adv_points, y,
                              out_folder_images, out_img)

out_imgs = [[eps, "images/{}.png".format(x)] for eps, x in zip(epsilons, out_imgs)]

template_vars = {"title": "Security Evaluation Report",
                 "sec_eval_plot": 'images/sec_eval_plot.png',
                 "stats": stats,
                 "out_imgs": out_imgs
                 }
html_out = template.render(template_vars)

from weasyprint import HTML

HTML(string=html_out, base_url=out_folder)\
    .write_pdf(os.path.join(out_folder, "report.pdf"), presentational_hints=True)
