# Security Evaluation API

Security evaluation module - ALOHA.eu project. http://aloha-h2020.eu

## Index

- Security Evaluation API
  * [Start the server](#start-the-server)
    + [Running without Docker](#running-without-docker)
    + [Running with Docker](#running-with-docker)
  * [Job queue handling](#job-queue-handling)
    + [How to start a security evaluation job](#how-to-start-a-security-evaluation-job)
    + [Job status API](#job-status-api)
    + [Job results API](#job-results-api)
  * [Standalone Tool](#standalone-tool)


## Start the server

### Running without Docker

Install the following tools:
* [MongoDB](https://www.mongodb.com)
* [Redis](https://redis.io/)

Then, install the Python requirements, running the following command in your shell:

```bash
pip install -r requirements.txt
```

Make sure both MongoDB and Redis server are running on your local machine. Test MongoDB connection by running the following command:

```bash
mongo
```

If the shell correctly connects to the MongoDB server, the mongo shell should appear. Exit the shell with `CTRL+D` or with the `quit()` command.

Now test Redis connection.

```bash
redis-cli ping
```

The response `PONG` should appear in the shell.

If one of the database servers is down, check the linked docs for finding out how to restart it in your system.

Notice: the code is expected to connect to the database through their default ports, 27018 for Mongo and 6379 for Redis. 

Now we are ready to start the server. Don't forget that this system uses external workers to process the long-running tasks, so we need to start the workers along with the sever. Run the following commands from the `app` folder:

```bash
python3 worker.py
```

Now open another shell and run the server:

```bash
python3 __init__.py
```

### Running with Docker

In order to use the docker-compose file provided, install [Docker](https://www.docker.com/) and start the server.

Docker will automatically take care of the setup process. Just type the following commands in your shell, from the `app` path:

```bash
docker-compose build
docker-compose up
```

If you want to use more workers, the following command should be used (replace the 2 with the number of workers you want to setup):

```bash
docker-compose up --scale worker=2
```

## Job queue handling

This project uses [Redis-RQ](http://python-rq.org/) for handling the queue of requested jobs. Please install [Redis](https://redis.io/) if you plan to run this Flask server without using Docker. 

## How to start a security evaluation job
A security evaluation job can be enqueued with a `POST` request to `/api/security_evaluations`. The API returns the **job unique ID** that can be used to access **job status** and **results**. Running workers will wait for new jobs in the queue and consume them with a FIFO rule.

The request should specify the following parameters in its body:
* **dataset** (*string*): the path where to find the dataset to be loaded (validation dataset should be used, otherwise check out the "indexes" input parameter).
* **trained-model** (*string*): the path of the onnx trained model.
* **performance-metric** (*string*): the performance metric type that should be used to evaluate the system adversarial robustness. Currently implemented only the `classification-accuracy` metric.
* **perturbation-type** (*string*): type of perturbation to apply (available: "max-norm" or "random").
* **perturbation-values** (*Array of floats*): array of values to use for crafting the adversarial examples. These are specified as percentage of the input range, fixed, in [0, 1] (*e.g.*, a value of 0.05 will apply a perturbation of maximum 5% of the input scale).
* **task** (*string*): type of task that the model is supposed to perform. This determines the attack scenario. (available: "classification", "detection", ~~"segmentation"~~)
* **evaluation-mode** (*string*): one of 'fast', 'complete'. A fast evaluation will perform the experiment with a subset of the whole dataset (100 samples). For more info on the fast evaluation, see [this paper](https://dl.acm.org/doi/10.1145/3310273.3323435).
* **indexes** (*Array of ints*): if the list of indexes is specified, it will be used for creating a specific sample from the dataset.
* **config-path** (*string*): Path of the configuration file needed for correctly preparing the model. Currently required only for YOLO anchors. If no value is passed, the configuration will not be considered.
* **pipeline-path** (*string*): File containing the specifications for the preprocessing pipeline. If no value is passed, the preprocessing will be ignored (a standard ToTensor will be applied).

```json
{
  "dataset": "<dataset-path>.hdf5",
  "trained-model": "<model_path>.onnx",
  "performance-metric": "classification-accuracy",
  "perturbation-type": "max-norm",
  "evaluation-mode": "fast",
  "task": "classification",
  "perturbation-values": [
    0, 0.01, 0.02, 0.03, 0.04, 0.05
  ],
  "pipeline-path": "/path/to/pipeline.json"
}

```

### Job status API
Job status can be retrieved by sending a `GET` request to `/api/security_evaluations/{id}`, where the id of the job should be replaced with the job ID of the previous point. A `GET` to `/api/security_evaluations` will return the status of all jobs found in the queues and in the finished job registries. 

### Job results API
Job results can be retrieved, once the job has entered the `finished` state, with a `GET` request to `/api/security_evaluations/{id}/output`. A request to this path with a job ID that is not yet in the `finished` status will redirect to the job status API.

The system manager can specify a `job_timeout` and a `result_ttl` in the configuration file. The `job_timeout` (expressed in seconds) prevents the worker to be blocked in the same job, giving the job a maximum time limit to complete. The `result_ttl` (expressed in seconds) specifies for how long the server should keep the results of the given job, preventing the database to grow unconditionally.

## How to inspect an adversarial example with different levels of perturbation
A adversarial example creation job can be enqueued with a `POST` request to `/api/adversarial_examples`. The API returns the **job unique ID** that can be used to access **job status** and **results**. Running workers will wait for new jobs in the queue and consume them with a FIFO rule.

The request should specify the following parameters in its body:
* **dataset** (*string*): the path where to find the dataset to be loaded (validation dataset should be used, otherwise check out the "indexes" input parameter).
* **trained-model** (*string*): the path of the onnx trained model.
* **performance-metric** (*string*): the performance metric type that should be displayed. Currently implemented only the `scores` metric.
* **perturbation-type** (*string*): type of perturbation to apply (available: "max-norm" or "random").
* **perturbation-values** (*Array of floats*): array of values to use for crafting the adversarial examples. These are specified as percentage of the input range, fixed, in [0, 1] (*e.g.*, a value of 0.05 will apply a perturbation of maximum 5% of the input scale).
* **task** (*string*): type of task that the model is supposed to perform. This determines the attack scenario. (available: "classification", "detection", "segmentation")
* **config-path** (*string*): Path of the Algorithm configuration file needed for correctly preparing the model. Currently required only for YOLO anchors. If no value is passed, the configuration will not be considered.
* **pipeline-path** (*string*): File containing the specifications for the preprocessing pipeline. If no value is passed, the preprocessing will be ignored (a standard ToTensor will be applied). Currently required only for classification use case.

```json

{
  "dataset": "<dataset-path>.hdf5",
  "trained-model": "<model_path>.onnx",
  "performance-metric": "scores",
  "perturbation-type": "max-norm",
  "task": "classification",
  "perturbation-values": [
    0, 0.01, 0.02, 0.03, 0.04, 0.05
  ],
  "pipeline-path": "<pipeline-path>.json"
}

```

```json

{
  "dataset": "<dataset-path>.hdf5",
  "trained-model": "<model_path>.onnx",
  "performance-metric": "map",
  "perturbation-type": "max-norm",
  "evaluation-mode": "fast",
  "task": "detection",
  "perturbation-values": [
    0, 0.001, 0.002, 0.003
  ],
  "config-path": "<config-path>.json"
}


```

### Job status API
Job status can be retrieved by sending a `GET` request to `/api/adversarial_examples/{id}`, where the id of the job should be replaced with the job ID of the previous point. A `GET` to `/api/adversarial_examples` will return the status of all jobs found in the queues and in the finished job registries. 

### Job results API
Job results can be retrieved, once the job has entered the `finished` state, with a `GET` request to `/api/adversarial_examples/{id}/output`. A request to this path with a job ID that is not yet in the `finished` status will redirect to the job status API.

The system manager can specify a `job_timeout` and a `result_ttl` in the configuration file. The `job_timeout` (expressed in seconds) prevents the worker to be blocked in the same job, giving the job a maximum time limit to complete. The `result_ttl` (expressed in seconds) specifies for how long the server should keep the results of the given job, preventing the database to grow unconditionally.

## Standalone tool
The Security Evaluation tool can be used also as a standalone module. 
For testing the use cases, run the script `security_evaluation_demo.py`. For seeing all 
the possible inputs, run:

```shell
python security_evaluation_demo.py --help
```

```text
usage: security_evaluation_demo.py [-h]
                                   [--task {classification,detection,segmentation}]
                                   [--evaluation-mode {fast,complete}]
                                   [--noise-type {random,max-norm}]
                                   [--target-dir TARGET_DIR]

Runs the demo of the security evaluation module.

optional arguments:
  -h, --help            show this help message and exit
  --task {classification,detection,segmentation}
                        Selection of the use case.
  --evaluation-mode {fast,complete}
                        Selection of the evaluation mode. Fast will run the
                        evaluation on a subset of samples and eps values.
  --noise-type {random,max-norm}
                        Selection of the perturbation.
  --target-dir TARGET_DIR
                        Directory where to store temporary data and final
                        results.

```

Specify a directory in `target-dir` to find there the output files. The 
script will download all files necessary to the experiments, then 
create a pdf report with the evaluation and with some example image.
A longer description of the parameters can be found in the API documentation.


For example, for running an adversarial max-norm attack in the classification use-case, 
run the following command:

```shell
python security_evaluation_demo.py --task classification --evaluation-mode fast --noise-type max-norm --target-dir ./temp
```

And the output, along with the temporary files required for instantiating the model 
and getting the samples from the dataset, will be placed in the `./temp` directory. 

Example output files can be found here:

* classification
  * [max-norm](./reports/classification/max-norm/report.pdf)
  * [random](./reports/classification/random/report.pdf)
  
* detection
  * [max-norm](./reports/detection/max-norm/report.pdf)
  * [random](./reports/detection/random/report.pdf)
  
* segmentation
  * [max-norm](./reports/segmentation/max-norm/report.pdf)
  * [random](./reports/segmentation/random/report.pdf)
  
