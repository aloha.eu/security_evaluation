# -*- coding: utf-8 -*-
from __future__ import absolute_import

from flask import Flask, url_for, redirect
from optparse import OptionParser

import api
from db.mongo import MongoConnector
from models.models import User


def create_app(environ="dev", start_response=""):
    app = Flask(__name__, static_folder='static')
    app.register_blueprint(
        api.bp,
        url_prefix='/api')

    @app.route('/')
    def home():
        return redirect(url_for('static', filename='swagger-ui/index.html'))

    # settings.get_settings(app)

    try:
        mongo = MongoConnector(app)

        user = User()
        user.email = "David.Solans@ca.com"

        user.password = "hidden_pass"
        user.projects = ['3']
        user.save()

        app.logger.info("Password: ", user.password)
    except Exception as ex:
        app.logger.info(str(ex))
        app.logger.info("User %s was already created" % user.email)

    return app


def new_app(*args, **kwargs):
    my_app = create_app()

    return my_app(*args, **kwargs)


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-p", "--port", dest="port",
                      help="Flask port", metavar="", default=8080)

    (options, args) = parser.parse_args()

    option_dict = vars(options)
    port = int(option_dict['port'])

    app = create_app()
    app.run(debug=False, port=port, host='0.0.0.0', threaded=True)
