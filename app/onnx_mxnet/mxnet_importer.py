import mxnet as mx
import mxnet.contrib.onnx as onnx_mxnet


class MXONNXLoader:
    """Loads a ONNX model as mxnet model."""

    def __init__(self, onnx_path, device=None):
        """
        Loads an onnx model for inference and for performing security evaluation. Wraps a mxnet model
        with methods predict  for inference and loss_gradient for computing gradients with respect to
        input (used for performing the adversarial attacks).

        :param onnx_path: path of the onnx file to use
        :param device: use mx.gpu() for using GPU. Default is mx.cpu()
        """
        self.onnx = onnx_path
        self.device = device
        if self.device is None:
            self.device = mx.cpu() if len(mx.test_utils.list_gpus()) == 0 else mx.gpu()
        self.load_model()

    def load_model(self):
        """
        Loads the model from the path specified during initialization.

        :return: None
        """
        sym, arg_params, aux_params = onnx_mxnet.import_model(self.onnx)
        model_metadata = onnx_mxnet.get_model_metadata(self.onnx)
        data_names = [inputs[0] for inputs in model_metadata.get('input_tensor_data')]
        net = mx.gluon.nn.SymbolBlock(outputs=sym, inputs=mx.sym.var(data_names[-1]))
        net_params = net.collect_params()
        for param in arg_params:
            if param in net_params:
                net_params[param]._load_init(arg_params[param], ctx=self.device)
        for param in aux_params:
            if param in net_params:
                net_params[param]._load_init(aux_params[param], ctx=self.device)
        net.hybridize()
        self.model = net
        return self.model

    def mx_array(self, x):
        """
        Converts an input numpy array to a mxnet array.

        :param x: input numpy array
        :return: mxnet array, loaded in the device specified during initialization
        """
        return mx.nd.array(x, ctx=self.device)

    def predict(self, x, return_decision_function=False):
        """
        Returns the prediction for the batch of samples x.

        :param return_decision_function: if set to True, the method returns the output scores
            along with the prediction results
        :param x: input batch. Should be a numpy array of size (batch_size, *input_shape)

        :return y: predictions
        :return scores: logits (returned only if return_decision_function is set to True)
        """
        x = self.mx_array(x)
        outputs = self.model(x)
        pred_label = mx.ndarray.argmax(outputs, axis=1)
        if return_decision_function is True:
            return outputs.asnumpy(), pred_label.asnumpy()
        # else return only the labels
        return pred_label.asnumpy()

    def loss_gradient(self, x, y, criterion=None, targeted=False):
        """
        Computes the gradient of the loss with respect to the input x.
        :param x: sample input
        :param y: correct labels
        :param criterion: criterion to use for computing the gradients. Default will be
            cross entropy loss
        :param targeted: if set to True, gradients will be relative to the target class y.
            Default value is False, which computes the gradient of the loss relative to the
            original label y

        :return: array gradients of the shape of x
        """
        x = self.mx_array(x)
        y = self.mx_array(y)

        x.attach_grad()

        if criterion is None:
            criterion = mx.gluon.loss.SoftmaxCrossEntropyLoss()
        with mx.autograd.record(train_mode=True):
            outputs = self.model(x)
            if targeted:
                loss = criterion(outputs, y)
            else:
                loss = -criterion(outputs, y)
        loss.backward()
        return x.grad.asnumpy()

    def _validate_input(self):
        """Checks format and size of input."""
        # TODO implement this check and return warning if it fails
        #   eventually, apply the _transform method for fixing input
        pass

    def _transform(self):
        """Transforms the input in order to be compliant with the model input shape."""
        # TODO (maybe) move to data_loader section
        pass
