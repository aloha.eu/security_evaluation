"""
Computes accuracy on yolo
"""

import json

import torch

from security_evaluations.evaluation_manager import EvaluationManager

splits_file = "/home/maurapintor/PycharmProjects/" \
              "object_detection_tests/people_and_cars/" \
              "train_valid_split.json"

with open(splits_file, 'r') as f:
    splits = json.load(f)

em = EvaluationManager(
    dataset_id="/home/maurapintor/PycharmProjects/unet_lgg/UNet_on_LGG/dataset/test_maxq_projectID"
               "-5fc5197036d89c9edae493d1_pipeline-5f57b66a8827460e38c03e36.hdf5",
    model_id='/home/maurapintor/PycharmProjects/unet_lgg/UNet_on_LGG/onnx_storage/onnx_model.onnx',
    metric='iou',
    perturbation_type='max-norm',
    evaluation_mode='fast',
    task='segmentation',
    perturbation_values=[0, 0.1, 0.3]
)

loader = em._validation_loader
model = em._model

print(em.sec_eval_curve())

def show_image(x):
    x = x.squeeze()
    if x.ndim == 3:
        x = x.permute(2, 1, 0)
    plt.imshow(x.detach().numpy())
    plt.xticks([])
    plt.yticks([])

from torch import nn
import matplotlib.pyplot as plt

def attack_image(images, model, steps, eps):
    stepsize = 0.001
    perturbed = images
    for i in range(steps):
        perturbed.requires_grad = True
        out = model(perturbed)
        out = nn.Sigmoid()(out).sum()
        out.backward()
        grad = perturbed.grad
        perturbed = perturbed - grad * stepsize
        perturbed = (perturbed - images).clamp(- eps, eps) + images
        perturbed = perturbed.detach()
    return perturbed

for i, (images, labels) in enumerate(loader):

    out = model(images)
    out = nn.Sigmoid()(out)
    attacked = attack_image(images, model, 10, eps=0.1)
    attacked_out = model(attacked)
    attacked_out = nn.Sigmoid()(attacked_out).contiguous()

    plt.figure(figsize=(15, 3))
    plt.subplot(1, 5, 1)
    show_image(images)
    plt.title("Original image")
    plt.subplot(1, 5, 2)
    show_image(labels)
    plt.title("Ground truth")
    plt.subplot(1, 5, 3)
    show_image(out)
    plt.title("Prediction (original)")
    plt.subplot(1, 5, 4)
    show_image(attacked)
    plt.title("Perturbed image")
    plt.subplot(1, 5, 5)
    show_image(attacked_out)
    plt.title("Prediction (perturbed)")
    plt.tight_layout()
    plt.show()
    if i == 2:
        break