from security_evaluations.evaluation_manager import EvaluationManager

em = EvaluationManager(
    dataset_id="/home/maurapintor/Documents/pluribus/ALOHA/data/aloha-kws/kws10_16x32_88acc/kws10_16x32_88acc.hdf5",
    model_id='/home/maurapintor/Documents/pluribus/ALOHA/data/aloha-kws/kws10_16x32_88acc/onnx_model.onnx',
    metric='classification-accuracy',
    perturbation_type='max-norm',
    evaluation_mode='fast',
    task='classification'
)

res = em.sec_eval_curve()
print(res)
