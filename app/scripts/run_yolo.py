"""
Computes accuracy on yolo
"""

import json

from security_evaluations.evaluation_manager import EvaluationManager

splits_file = "/home/maurapintor/PycharmProjects/" \
              "object_detection_tests/people_and_cars/" \
              "train_valid_split.json"

with open(splits_file, 'r') as f:
    splits = json.load(f)

em = EvaluationManager(
    dataset_id="/home/maurapintor/PycharmProjects/object_detection_tests/people_and_cars/test_pke_projectID-5f9438f1cc12d3746230ff4e_pipeline-5f6cc9dacc2cfec2c032c7ef.hdf5",
    model_id='/home/maurapintor/PycharmProjects/object_detection_tests/people_and_cars/onnx_model.onnx',
    metric='map',
    perturbation_type='max-norm',
    evaluation_mode='fast',
    task='detection',
    perturbation_values=[0, 0.001, 0.002, 0.003]
)

loader = em._validation_loader
model = em._model

print(em.sec_eval_curve())
