from werkzeug.security import check_password_hash, generate_password_hash
from flask_mongoengine import MongoEngine
import datetime
import jwt
from mongoengine.fields import *
from settings import settings
import jwt

db = MongoEngine()


class Preset(db.Document):
    title = db.StringField(max_length=80, unique=True)
    description = db.StringField(max_length=255)
    creation_date = db.DateTimeField(default=datetime.datetime.now)
    modified_date = db.DateTimeField(default=datetime.datetime.now)
    owner = db.ReferenceField('User')
    groups = db.ListField(db.ReferenceField('Group'), default=[])
    public = db.BooleanField(default=True)
    algo_folder = db.StringField()
    data_set_name = db.StringField()
    data_folder = db.StringField()
    data_file = db.StringField()
    batch_size = db.IntField()
    loss = db.StringField()
    image_width = db.IntField()
    image_height = db.IntField()
    image_number_of_channels = db.IntField()
    num_classes = db.IntField()
    task_type = db.StringField()
    accuracy= db.StringField()


class Code(db.DynamicEmbeddedDocument):
    path = db.StringField()
    status = db.StringField()
    creation_date = db.DateTimeField(default=datetime.datetime.now)



class Step(db.DynamicEmbeddedDocument):
    active = db.BooleanField()
    status = db.StringField()
class Project(db.DynamicDocument):

    creator = db.StringField()
    title   = db.StringField()
    creation_date = db.DateTimeField(default=datetime.datetime.now)
    modified_date = db.DateTimeField(default=datetime.datetime.now)

    sec_en = db.EmbeddedDocumentField('Step')
    tra_en = db.EmbeddedDocumentField('Step')
    rpi_en = db.EmbeddedDocumentField('Step')
    per_en = db.EmbeddedDocumentField('Step')

    sesame_en = db.BooleanField()
    PI_en = db.BooleanField()

    # each project has only one dataset, each dataset could have many projects
    dataset      = db.StringField()

    # each project has only one architecture, each architecture could have many projects
    architecture = db.StringField()
    model = db.StringField() #this is the new Algorithm configuration in the front-end, trained in step 1 and choose from results.


    groups = db.ListField(db.ReferenceField('Group'))

    code = db.MapField(db.EmbeddedDocumentField('Code'))

    def clean(self):
        self.modified_date = datetime.datetime.now()

    def get_id(self):
        return str(self.id)

    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)


class User(db.Document):

    email = db.StringField(max_length=120, required=True, unique=True)
    password = db.StringField(required=True)
    active = db.BooleanField(default=True)
    name = db.StringField(max_length=120)
    surname = db.StringField(max_length=120)
    username = db.StringField(max_length=120)
    projects = db.ListField(db.StringField())
    creation_date = db.DateTimeField(default=datetime.datetime.now)
    modified_date = db.DateTimeField(default=datetime.datetime.now)
    admin = db.BooleanField(default=False)
    group_owner = db.ListField(db.ReferenceField('Group'), default=[])
    preset_owner = db.ListField(db.ReferenceField('Preset'), default=[])
    groups = db.ListField(db.ReferenceField('Group'), default=[])
    hashcode = db.StringField(default=None)

    # Flask-Login integration
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)

    def password_hash(password):
        return generate_password_hash(password)

    @staticmethod
    def validate_login(password_hash, password):
        return check_password_hash(password_hash, password)

    def encode_auth_token(self, user_id):
        """
        Generates the Auth Token
        :return: string
        """
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=7, seconds=5),
                'iat': datetime.datetime.utcnow(),
                'sub': user_id
            }
            return jwt.encode(
                payload,
                settings.get_secret_key(),
                algorithm='HS256'
            )
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(auth_token):
        """
        Decodes the auth token
        :param auth_token:
        :return: integer|string
        """
        try:
            payload = jwt.decode(auth_token, settings.get_secret_key())
            is_blacklisted_token = BlacklistToken.check_blacklist(auth_token)
            if is_blacklisted_token:
                return 'Token blacklisted. Please log in again.'
            else:
                return payload['sub']
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again.'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in again.'



class BlacklistToken(db.Document):
    token = StringField(max_length=500, required=True, unique=True)
    blacklisted_on = db.DateTimeField(default=datetime.datetime.now)

    def get_id(self):
        return str(self.id)

    @staticmethod
    def check_blacklist(auth_token):
        # check whether auth token has been blacklisted
        res = BlacklistToken.objects(token=str(auth_token)).first()
        if res:
            return True
        else:
            return False

class Group(db.Document):
    name = db.StringField(max_length=80, unique=True)
    description = db.StringField(max_length=255)
    owner = db.ReferenceField('User')
    projects = db.ListField(db.ReferenceField('Project', reverse_delete_rule=db.PULL), default=[])
    users = db.ListField(db.ReferenceField('User', reverse_delete_rule=db.PULL), default=[])
    creation_date = db.DateTimeField(default=datetime.datetime.now)
    modified_date = db.DateTimeField(default=datetime.datetime.now)
    presets = db.ListField(db.ReferenceField('Preset', reverse_delete_rule=db.PULL))
    #http://docs.mongoengine.org/guide/defining-documents.html#one-to-many-with-listfields

User.register_delete_rule(Group, 'owner', db.DENY)
User.register_delete_rule(Preset, 'owner', db.DENY)
Group.register_delete_rule(Project, 'groups', db.DENY)
Group.register_delete_rule(Preset, 'groups', db.DENY)
Group.register_delete_rule(User, 'groups', db.DENY)

class Card(db.Document):
    title = StringField(max_length=32)
    label = StringField(max_length=32)
    description = StringField(max_length=500)
    laneId = ObjectIdField(required=False)
    projectId = ObjectIdField(required=False)

    def get_id(self):
        return str(self.id)


class Lane(db.Document):
    position = IntField()
    title = StringField(max_length=32)
    cards = ListField(ReferenceField(Card, required=False))

    def get_id(self):
        return str(self.id)

#Dataset
class Dataset(db.Document):
    creator       = db.StringField()
    creation_date = db.DateTimeField(default=datetime.datetime.now)
    modified_date = db.DateTimeField(default=datetime.datetime.now)

    name = db.StringField()
    project = db.StringField() #deprecated

    name            = db.StringField()
    path            = db.StringField()
    samples_id      = db.ListField()
    number_labels   = db.IntField()
    slice_split     = db.ListField()
    features        = db.ListField()
    data_resolution = db.ListField() #Should be a matrix
    encryption_type = db.StringField() #TO be checked
    label_x         = db.ListField() # Array
    label_y         = db.ListField() #Array
    training_flag   = db.ListField()

    def clean(self):
        self.modified_date = datetime.datetime.now()

    def get_id(self):
        return str(self.id)

    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)


# Constraints use case
class ProjectConstraints(db.Document):
    creation_date = db.DateTimeField(default=datetime.datetime.now)
    modified_date = db.DateTimeField(default=datetime.datetime.now)


    project       = db.StringField()


    power_value             = db.FloatField()
    power_priority          = db.IntField()

    security_value          = db.IntField()
    security_priority       = db.IntField()

    execution_time_value    = db.FloatField()
    execution_time_priority = db.IntField()

    accuracy_value          = db.FloatField()
    accuracy_priority       = db.IntField()


    def clean(self):
        self.modified_date = datetime.datetime.now()

    def get_id(self):
        return str(self.id)


    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)

#Conf file
class ConfigurationFile(db.Document):
    creation_date    = db.DateTimeField(default=datetime.datetime.now)
    modified_date    = db.DateTimeField(default=datetime.datetime.now)

    project          = db.StringField()

    data_set_name    = db.StringField()
    data_folder      = db.StringField()
    data_file        = db.StringField()

    algo_folder      = db.StringField()

    mode             = db.StringField()
    lr               = db.FloatField()
    lr_decay         = db.FloatField()
    ref_steps        = db.IntField()
    ref_patience     = db.IntField()
    batch_size       = db.IntField()
    num_epochs       = db.IntField()
    loss             = db.StringField()
    optimizer        = db.StringField()
    image_dimensions = db.ListField()
    num_classes      = db.IntField()
    num_filters      = db.IntField()
    task_type        = db.StringField()
    accuracy         = db.StringField()
    augmentation     = db.DictField()
    data_split       = db.FloatField()
    cross_val        = db.IntField()
    normalize        = db.BooleanField()
    zero_center      = db.BooleanField()

    # path = db.StringField()


    def get_id(self):
        return str(self.id)


    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)


class AlgorithmConfiguration(db.Document):
    creation_date   = db.DateTimeField(default=datetime.datetime.now)
    modified_date   = db.DateTimeField(default=datetime.datetime.now)

    project         = db.StringField()
    onnx            = db.StringField() # the path of the onnx model before the training
    pytorch_net     = db.StringField()


    # training
    onnx_trained        = db.StringField() # the path of the trained onnx model
    pytorch_trained     = db.StringField() # the path of the trained pytotch model

    training_accuracy   = db.FloatField()
    training_loss       = db.FloatField()
    validation_accuracy = db.FloatField()
    validation_loss     = db.FloatField()

    training_log        = db.ListField()
    execution_time      = db.ListField()

    # refinement parsimonious inference
    rpi_onnx_path           = db.StringField() # the path of the onnx model after the rpi
    rpi_training_accuracy   = db.ListField()
    rpi_training_loss       = db.ListField()
    rpi_quantization        = db.ListField()

    rpi_best_quantization   = db.DictField()


    # power performance evaluation
    performance    = db.FloatField()
    energy         = db.FloatField()
    processors     = db.FloatField()
    memory         = db.FloatField()


    # security
    sec_level      = db.StringField() #low, medium, high
    sec_value      = db.FloatField()
    sec_curve      = db.DictField()


    # images from tools
    training_img_path        = db.StringField()
    power_perf_eval_img_path = db.StringField()
    rpi_img_path             = db.StringField()
    security_img_path        = db.StringField()

    # logs from tools
    training_log_path        = db.StringField()
    power_perf_eval_log_path = db.StringField()
    rpi_log_path             = db.StringField()
    security_log_path        = db.StringField()


    #######
    #SL DSE
    #######

    #SESAME
    selected_for_SL_DSE = db.BooleanField()
    sesame_map_list     = db.ListField()
    sesame_pareto       = db.StringField()
    sesame_perf_cost    = db.StringField()

    #PI



    def clean(self):
        self.modified_date = datetime.datetime.now()

    def get_id(self):
        return str(self.id)


    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)




class Architecture(db.Document): #File
    creation_date = db.DateTimeField(default=datetime.datetime.now)
    modified_date = db.DateTimeField(default=datetime.datetime.now)


    name = db.StringField()

    path = db.StringField()
    project = db.StringField() #deprecated

    def clean(self):
        self.modified_date = datetime.datetime.now()

    def get_id(self):
        return str(self.id)

    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)


class Image(db.Document):
    creation_date = db.DateTimeField(default=datetime.datetime.now)
    modified_date = db.DateTimeField(default=datetime.datetime.now)

    path = db.StringField()
    project = db.StringField()

    def clean(self):
        self.modified_date = datetime.datetime.now()

    def get_id(self):
        return str(self.id)

    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)
