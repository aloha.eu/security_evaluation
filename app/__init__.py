# -*- coding: utf-8 -*-
from __future__ import absolute_import
from flask import Flask, url_for, redirect
import api
from settings import settings
from optparse import OptionParser


def create_app():

    app = Flask(__name__, static_folder='static')
    app.register_blueprint(
        api.bp,
        url_prefix='/api')

    @app.route('/')
    def home():
        return redirect(url_for('static', filename='swagger-ui/index.html'))

    settings.get_settings(app)

    return app


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-p", "--port", dest="port",
                      help="Flask port", metavar="", default=5000)

    (options, args) = parser.parse_args()

    option_dict = vars(options)
    port = int(option_dict['port'])

    create_app().run(debug=False, port=port, host='0.0.0.0', threaded=True)
