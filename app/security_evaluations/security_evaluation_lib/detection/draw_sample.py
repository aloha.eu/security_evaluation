import os

import cv2
import matplotlib.pyplot as plt
import numpy as np

from security_evaluations.security_evaluation_lib.detection.yolo_acc import predictions


def draw_rectangles(data, classes,
                    model=None, yolo_anchors=None,
                    conf_threshold=None, nms_threshold=None, labels=None):
    if model:
        logits = model.forward(data)
        out = predictions(logits, anchors=yolo_anchors, image_size=416,
                          conf_threshold=conf_threshold,
                          nms_threshold=nms_threshold)
    elif labels is not None:
        out = labels
    else:
        raise ValueError("Either model or labels should be defined.")

    im = np.float32(data[0, ...].permute((1, 2, 0)).detach().numpy())
    im = cv2.UMat(im).get()

    if len(out):
        assert len(out) == 1
        objects = out[0]
        for obj in range(objects.shape[0]):
            if model:
                box_coords, _, y = objects[obj, :4], objects[obj, 4], objects[obj, -1]
            else:
                box_coords, y = objects[obj, :4], objects[obj, -1]
            box_coords = box_coords.tolist()
            box_coords = [int(b) for b in box_coords]
            y_pred = int(y)
            im = cv2.rectangle(im,
                               (box_coords[0], box_coords[1]),
                               (box_coords[0] + box_coords[2],
                                box_coords[1] + box_coords[3]),
                               (255, 255, 255), 2)
            im = cv2.putText(im, classes[y_pred], (box_coords[0], box_coords[1]),
                             cv2.FONT_HERSHEY_COMPLEX, 0.8, (255, 255, 255))
    return im


def draw_sample(data, perturbed, labels, model, classes, yolo_anchors,
                conf_threshold, nms_threshold, figure_path, figure_name):
    im_clean = draw_rectangles(data, classes, model, yolo_anchors, conf_threshold, nms_threshold, labels)
    im_adv = draw_rectangles(perturbed, classes, model, yolo_anchors, conf_threshold, nms_threshold, labels)
    gt = draw_rectangles(data, classes, labels=labels)
    perturbation = data - perturbed
    perturbation /= abs(perturbation).max()
    perturbation = perturbation.squeeze(0) \
        .permute((1, 2, 0)).detach().numpy()

    plt.figure(figsize=(20, 5))
    plt.subplot(1, 4, 1)
    plt.title("ground truth")
    plt.imshow(gt)
    plt.xticks([])
    plt.yticks([])
    plt.subplot(1, 4, 2)
    plt.imshow(im_clean)
    plt.title("predictions")
    plt.xticks([])
    plt.yticks([])
    plt.subplot(1, 4, 3)
    plt.imshow(im_adv)
    plt.title("attacked")
    plt.xticks([])
    plt.yticks([])
    plt.subplot(1, 4, 4)
    plt.imshow(perturbation, vmin=-1, vmax=1)
    plt.title("perturb")
    plt.xticks([])
    plt.yticks([])
    plt.savefig(os.path.join(figure_path, "{}.pdf").format(figure_name), format='pdf')
    plt.savefig(os.path.join(figure_path, "{}.png").format(figure_name), format='png')
    plt.close()
