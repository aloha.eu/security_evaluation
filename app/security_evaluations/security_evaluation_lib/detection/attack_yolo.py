from security_evaluations.attack_base import AttackBase
from security_evaluations.security_evaluation_lib.detection.compute_mAP import mAP_metric
from security_evaluations.security_evaluation_lib.detection.draw_sample import draw_sample
from security_evaluations.security_evaluation_lib.detection.yolo_acc import predictions


class AttackDetection(AttackBase):
    def __init__(self, model, lb, ub,
                 yolo_anchors, steps=1, step_size=0.1,
                 yolo_classes=('person', 'car'),
                 conf_threshold=0.25, nms_threshold=0.3,
                 iou_threshold=0.3,
                 remove=True):
        self.yolo_anchors = yolo_anchors
        self.n_yolo_anchors = len(yolo_anchors)
        self.steps = steps
        self.step_size = step_size
        self.remove = remove
        self.classes = yolo_classes
        self.num_classes = len(yolo_classes)
        self.conf_threshold = conf_threshold
        self.nms_threshold = nms_threshold
        self.iou_threshold = iou_threshold
        super(AttackDetection, self).__init__(model, lb, ub)

    def run(self, x, y, eps):
        x_adv = x
        for i in range(self.steps):
            x_adv.requires_grad = True
            logits = self.model(x_adv)

            if logits.dim() == 3:
                logits.unsqueeze_(0)
            w = logits.size(2)
            h = logits.size(3)
            logits = logits.view(1, self.n_yolo_anchors, -1, h * w)

            if self.remove:
                obj_scores = logits[:, :, 4, :].sigmoid().sum()
            else:
                obj_scores = logits[:, :, 4, :].sum()

            obj_scores.backward()
            x_adv = x_adv - self.step_size * x_adv.grad

            x_adv = (x_adv - x).clamp(- eps, eps) + x  # project
            x_adv = x_adv.clamp(0, 1)  # input space

            x_adv = x_adv.detach()

        return x_adv.cpu().detach()

    def evaluate_perf(self, x, labels):
        total_acc = 0
        for idx in range(x.shape[0]):
            data = x[idx, ...].unsqueeze(0)
            label = labels[idx, ...].unsqueeze(0)
            logits = self.model(data)
            out = predictions(logits, anchors=self.yolo_anchors,
                              image_size=416,
                              conf_threshold=self.conf_threshold,
                              nms_threshold=self.nms_threshold)

            total_acc += mAP_metric(out, label,
                                    n_classes=self.num_classes,
                                    iouThreshold=self.iou_threshold)
        acc = total_acc / x.shape[0]
        return acc

    def generate_figure(self, x, x_adv, y, figure_path, figure_name):
        draw_sample(x, x_adv, y, self.model, self.classes, self.yolo_anchors,
                    self.conf_threshold, self.nms_threshold, figure_path, figure_name)
