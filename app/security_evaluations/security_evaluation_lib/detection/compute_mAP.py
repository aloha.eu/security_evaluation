from security_evaluations.security_evaluation_lib.detection.BoundingBox import BoundingBox
from security_evaluations.security_evaluation_lib.detection.BoundingBoxes import BoundingBoxes
from security_evaluations.security_evaluation_lib.detection.Evaluator import Evaluator
from security_evaluations.security_evaluation_lib.detection.utils import BBType, BBFormat, CoordinatesType, \
    MethodAveragePrecision


def getBoundingBoxes(data_tensor,
                     isGT,
                     bbFormat,
                     coordType,
                     allBoundingBoxes=None,
                     allClasses=None,
                     imgSize=(0, 0)):
    """Read txt files containing bounding boxes (ground truth and detections)."""
    if allBoundingBoxes is None:
        allBoundingBoxes = BoundingBoxes()
    if allClasses is None:
        allClasses = []
    # Read ground truths
    # os.chdir(directory)
    # files = glob.glob("*.txt")
    # files.sort()
    # Read GT detections from txt file
    # Each line of the files in the groundtruths folder represents a ground truth bounding box
    # (bounding boxes that a detector should detect)
    # Each value of each line is  "class_id, x, y, width, height" respectively
    # Class_id represents the class of the bounding box
    # x, y represents the most top-left coordinates of the bounding box
    # x2, y2 represents the most bottom-right coordinates of the bounding box

    for tensor in data_tensor:
        if isGT:
            idClass = tensor[-1].item()  # class
            x = tensor[0].item()
            y = tensor[1].item()
            w = tensor[2].item()
            h = tensor[3].item()
            bb = BoundingBox(
                0,
                idClass,
                x,
                y,
                w,
                h,
                coordType,
                imgSize,
                BBType.GroundTruth,
                format=bbFormat)
        else:
            idClass = tensor[-1].item()  # class
            confidence = tensor[-2].item()
            x = tensor[0].item()
            y = tensor[1].item()
            w = tensor[2].item()
            h = tensor[3].item()
            bb = BoundingBox(
                0,
                idClass,
                x,
                y,
                w,
                h,
                coordType,
                imgSize,
                BBType.Detected,
                confidence,
                format=bbFormat)
        allBoundingBoxes.addBoundingBox(bb)
        if idClass not in allClasses:
            allClasses.append(idClass)
    return allBoundingBoxes, allClasses


def mAP_metric(y_pred, y_true, n_classes=None, iouThreshold=0.5, **kwargs):
    gtFormat = BBFormat.XYWH
    detFormat = BBFormat.XYWH
    gtCoordType = CoordinatesType.Absolute
    detCoordType = CoordinatesType.Absolute

    # compute mAP per image
    mAP_batch = 0
    mAP_all = []
    batch_size = len(y_true)

    if len(y_pred) > 0:
        for batch_id in range(batch_size):
            if len(y_pred[batch_id]) > 0:
                imgSize = (0, 0)
                # Get groundtruth boxes
                allBoundingBoxes, allClasses = getBoundingBoxes(
                    y_true[batch_id], True, gtFormat, gtCoordType, imgSize=imgSize)
                # Get detected boxes
                allBoundingBoxes, allClasses = getBoundingBoxes(
                    y_pred[batch_id], False, detFormat, detCoordType, allBoundingBoxes, allClasses, imgSize=imgSize)
                allClasses.sort()

                evaluator = Evaluator()
                acc_AP = 0
                validClasses = 0

                # Plot Precision x Recall curve
                detections = evaluator.PlotPrecisionRecallCurve(
                    allBoundingBoxes,  # Object containing all bounding boxes (ground truths and detections)
                    IOUThreshold=iouThreshold,  # IOU threshold
                    method=MethodAveragePrecision.EveryPointInterpolation,
                    showAP=True,  # Show Average Precision in the title of the plot
                    showInterpolatedPrecision=False,  # Don't plot the interpolated precision curve
                    savePath=None,
                    showGraphic=False)

                # each detection is a class
                for metricsPerClass in detections:

                    # Get metric values per each class
                    cl = metricsPerClass['class']
                    ap = metricsPerClass['AP']
                    precision = metricsPerClass['precision']
                    recall = metricsPerClass['recall']
                    totalPositives = metricsPerClass['total positives']
                    total_TP = metricsPerClass['total TP']
                    total_FP = metricsPerClass['total FP']

                    if totalPositives > 0:
                        validClasses = validClasses + 1
                        acc_AP = acc_AP + ap
                        prec = ['%.2f' % p for p in precision]
                        rec = ['%.2f' % r for r in recall]
                        ap_str = "{0:.2f}%".format(ap * 100)
                        # ap_str = "{0:.4f}%".format(ap * 100)
                        # print('AP: %s (%s)' % (ap_str, cl))

                if acc_AP > 0:
                    mAP = acc_AP / validClasses
                else:
                    mAP = 0
                # mAP_str = "{0:.2f}%".format(mAP * 100)
                # print('mAP: %s' % mAP_str)
                mAP_batch = mAP_batch + mAP
                mAP_all.append(mAP)
            else:
                mAP = 0
                mAP_all.append(mAP)
        return mAP_batch / batch_size
    else:
        mAP_all = 0
        return 0
