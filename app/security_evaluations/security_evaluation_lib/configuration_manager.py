from ...models.models import ConfigurationFile
from configparser import ConfigParser

class ConfigurationManager:

    def __init__(self, configuration_id):
        self._configuration_id = configuration_id
        self._configuration_metadata = ConfigurationFile.objects.get(id=configuration_id)
        self.load_configuration()

    def load_configuration(self):
        self._config = ConfigParser()
        self._config.read(self._configuration_metadata.path)

        # configuration data to read
        # TODO place here each configuration parameter
        self.job_timeout = self._config.getint('JOBS', 'JOB_TIMEOUT')
        self.result_ttl = self._config.getint('JOBS', 'RESULT_TTL')

        self.log_level = self._config.get('LOGGING', 'LOG_LEVEL')