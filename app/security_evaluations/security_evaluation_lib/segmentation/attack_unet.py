import os

from torch import nn

from security_evaluations.attack_base import AttackBase
from security_evaluations.security_evaluation_lib.detection.yolo_acc import IoU


class AttackSegmentation(AttackBase):
    def __init__(self, model, lb, ub, steps=1):
        self.steps = steps

        super(AttackSegmentation, self).__init__(model, lb, ub)

    def run(self, x, y, eps):
        stepsize = 0.01
        perturbed = x.detach()
        for i in range(self.steps):
            perturbed.requires_grad = True
            out = self.model(perturbed)
            out = nn.Sigmoid()(out).sum()
            out.backward()
            grad = perturbed.grad
            perturbed = perturbed - grad * stepsize
            perturbed = (perturbed - x).clamp(- eps, eps) + x
            perturbed = perturbed.detach()
        return perturbed

    def evaluate_perf(self, x, labels):
        total_acc = 0
        for idx in range(x.shape[0]):
            data = x[idx, ...].unsqueeze(0)
            label = labels[idx, ...].unsqueeze(0)
            y_pred = self.model(data)
            total_acc += IoU(y_pred, label)
        acc = total_acc / x.shape[0]
        return acc.item()

    def generate_figure(self, x, x_adv, y, figure_path, figure_name):
        import matplotlib.pyplot as plt

        def show_image(t, bounds=None):
            t = t.squeeze()
            if t.ndim == 3:
                t = t.permute(2, 1, 0)
            if bounds:
                plt.imshow(t.detach().numpy(), vmin=bounds[0], vmax=bounds[1])
            else:
                if t.sum() != 0:
                    t -= t.min()
                    t /= t.max()
                plt.imshow(t.detach().numpy())
            plt.xticks([])
            plt.yticks([])

        perturbation = x - x_adv
        perturbation /= abs(perturbation).max()
        perturbation = perturbation.squeeze()

        preds = nn.Sigmoid()(self.model(x)).contiguous()
        adv_preds = nn.Sigmoid()(self.model(x_adv)).contiguous()

        plt.figure(figsize=(20, 5))
        plt.subplot(1, 6, 1)
        plt.title("original image")
        show_image(x)
        plt.xticks([])
        plt.yticks([])
        plt.subplot(1, 6, 2)
        plt.title("ground truth")
        print(y)
        show_image(y)
        plt.xticks([])
        plt.yticks([])
        plt.subplot(1, 6, 3)
        show_image(preds)
        plt.title("predictions")
        plt.xticks([])
        plt.yticks([])
        plt.subplot(1, 6, 4)
        show_image(x_adv)
        plt.title("perturbed image")
        plt.xticks([])
        plt.yticks([])
        plt.subplot(1, 6, 5)
        show_image(adv_preds)
        plt.title("perturbed predictions")
        plt.xticks([])
        plt.yticks([])
        plt.subplot(1, 6, 6)
        show_image(perturbation, bounds=(-1, 1))
        plt.title("perturbation")
        plt.xticks([])
        plt.yticks([])
        plt.savefig(os.path.join(figure_path, "{}.pdf").format(figure_name), format='pdf')
        plt.savefig(os.path.join(figure_path, "{}.png").format(figure_name), format='png')
        plt.close()
