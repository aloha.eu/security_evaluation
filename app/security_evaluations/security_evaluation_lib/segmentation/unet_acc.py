from torch import nn


def IoU(y_pred, y_true, **kwargs):
    smooth = kwargs['smooth']
    dif = 0
    for i in range(y_pred.shape[0]):
        prd = nn.Sigmoid()(y_pred[i]).contiguous().view(-1)
        tar = y_true[i].contiguous().view(-1)
        int = (prd * tar).sum()
        dis = (int / (prd.sum() + tar.sum() - int + smooth))
        dif += dis
    return dif / y_pred.shape[0]
