import importlib
import os

import torch
from torch import nn

from onnxparser.onnxparser import onnxextract
from security_evaluations.security_evaluation_lib.models import MODEL_PATH


class TorchONNXLoader:
    """Loads a ONNX model as pytorch model."""

    def __init__(self, onnx_path, device=None, use_case='classification'):
        """
        Loads an onnx model for inference and for performing security evaluation. Wraps a pytorch model
        with methods predict  for inference and loss_gradient for computing gradients with respect to
        input (used for performing the adversarial attacks).

        :param onnx_path: path of the onnx file to use
        :param device: use 'cuda' for using GPU. Default is 'cpu'
        """
        self.onnx = onnx_path
        self.device = device
        if self.device is None:
            self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.model = None
        self._parsed_path = MODEL_PATH
        self.use_case = use_case
        self.load_model()

    def load_model(self):
        """
        Loads the model from the path specified during initialization.

        :return: None
        """
        # TODO parameters input h, input w, channels, classes
        onnxextract(self.onnx, path=self._parsed_path, target='pytorch')

        importlib.invalidate_caches()
        module = ".".join(os.path.relpath(self._parsed_path).split(os.path.sep))
        if module.startswith('...'):
            module = module[3:]
        module += '.Net'
        net = importlib.import_module(module)
        self.model = net.Net().to(self.device)
        return self.model

    def torch_tensor(self, x):
        """
        Converts an input numpy array to a pytorch tensor.

        :param x: input numpy array
        :return: torch tensor, loaded in the device specified during initialization
        """
        return torch.from_numpy(x).to(self.device)

    def predict(self, x, return_decision_function=False):
        """
        Returns the prediction for the batch of samples x.

        :param return_decision_function: if set to True, the method returns the output scores
            along with the prediction results
        :param x: input batch. Should be a numpy array of size (batch_size, *input_shape)

        :return y: predictions
        :return scores: logits (returned only if return_decision_function is set to True)
        """
        x = self.torch_tensor(x)
        outputs = self.model(x)
        pred_label = torch.argmax(outputs, dim=1)
        if return_decision_function is True:
            return outputs.cpu().detach().numpy(), pred_label.cpu().numpy()
        # else return only the labels
        return pred_label.cpu().numpy()

    def loss_gradient(self, x, y, criterion=None, targeted=False):
        """
        Computes the gradient of the loss with respect to the input x.
        :param x: sample input
        :param y: correct labels
        :param criterion: criterion to use for computing the gradients. Default will be
            cross entropy loss
        :param targeted: if set to True, gradients will be relative to the target class y.
            Default value is False, which computes the gradient of the loss relative to the
            original label y

        :return: array gradients of the shape of x
        """
        x = self.torch_tensor(x)
        y = self.torch_tensor(y)

        x.requires_grad = True

        if criterion is None:
            criterion = nn.CrossEntropyLoss()

        outputs = self.model(x)
        if targeted:
            loss = criterion(outputs, y)
        else:
            loss = -criterion(outputs, y)
        self.model.zero_grad()
        loss.backward()

        return x.grad.data.cpu().numpy()

    def _validate_input(self):
        """Checks format and size of input."""
        # TODO implement this check and return warning if it fails
        #   eventually, apply the _transform method for fixing input
        pass

    def _transform(self):
        """Transforms the input in order to be compliant with the model input shape."""
        # TODO (maybe) move to data_loader section
        pass
