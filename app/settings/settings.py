#Define your own settings here
import os
from logging.config import dictConfig

def get_prod_settings(app):
    # MongoDB settings
        app.config['MONGODB_SETTINGS'] = {
        'db': 'aloha',
        'host': os.environ['DB_PORT_27017_TCP_ADDR'],#Required to work on docker-compose
        'port': 27017,
        #'username':'aloha',
        #'password':'pwd123'
    }


def get_dev_settings(app):
    # MongoDB settings
    if "MONGO_DB_NAME" not in os.environ:
        print ("Error: MONGO_DB_NAME env var missing! Set the env variables for the Mongo DB connection")
        exit(1)

    if "MONGO_DB_TCP_ADDR" not in os.environ:
        print ("Error: MONGO_DB_TCP_ADDR env var missing! Set the env variables for the Mongo DB connection")
        exit(1)

    if "MONGO_DB_PORT" not in os.environ:
        print ("Error: MONGO_DB_PORT env var missing! Set the env variables for the Mongo DB connection")
        exit(1)
    print (os.environ['MONGO_DB_NAME'])
    print (os.environ['MONGO_DB_TCP_ADDR'])
    print (os.environ['MONGO_DB_PORT'])
    app.config['MONGODB_SETTINGS'] = {
        'db':   os.environ['MONGO_DB_NAME'],
        'host': os.environ['MONGO_DB_TCP_ADDR'],
        'port': int(os.environ['MONGO_DB_PORT']),
        #'username':'aloha',
        #'password':'pwd123'
    }


def get_settings(app):


    ###
    #    Modify this flag to change configuration
    ###
    environment = 'DEV' # You can use PROD instead.


    print("Configuring app for ["+environment+"] env")

    if environment == 'DEV':
        get_dev_settings(app)
    else:
        get_prod_settings(app)


    """
    # Logging settings
    ###                Check: http://flask.pocoo.org/docs/dev/logging/ to know more
    dictConfig({
        'version': 1,
        'formatters': {'default': {
            'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
        }},
        'handlers': {'wsgi': {
            'class': 'logging.StreamHandler',
            'stream': 'ext://flask.logging.wsgi_errors_stream',
            'formatter': 'default'
        }},
        'root': {
            'level': 'INFO',
            'handlers': ['wsgi']
        }
    })
    """
