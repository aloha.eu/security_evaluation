## Generate MongoDB models from YAML definitions
### Example: https://stackoverflow.com/questions/6311738/create-a-model-from-yaml-json-on-the-fly

from flask_mongoengine import MongoEngine
from mongoengine.connection import get_db, connect
import os


class MongoConnector():

    def __init__(self, app=None):
        if app is not None:
            self.mongo_engine = MongoEngine()
            self.mongo_engine.init_app(app)

            # Obtain db connection(s)
            self.db = get_db()
            # print("Database stats: ", self.db.command('dbstats'))
        else:
            db = os.environ.get('MONGO_DB_NAME', 'aloha-test')
            host = str(os.environ.get('MONGO_DB_TCP_ADDR', 'localhost'))
            port = int(os.environ.get('MONGO_DB_PORT', '27017'))
            self.mongo_engine = connect(
                db=db,
                host=host,
                port=port)
